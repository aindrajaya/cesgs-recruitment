import React from 'react';

import AuthUserContext from './context';
import {withFirebase} from '../Firebase';

const needsEmailVerification = authUser =>
    authUser &&
    !authUser.emailVerified &&
    authUser.providerData
      .map(provider => provider.providerId)
      .includes('password');

const withEmailVerification = Component => {
    class WithEmailVerification extends React.Component{
        constructor(props){
            super(props);

            this.state = {isSent: false};
        }

        onSendEmailVerification = () => {
            this.props.firebase
            .doSendEmailVerification()
            .then(() => this.setState({isSent:true}));
        };
        
        render(){
            return(
                <AuthUserContext.Consumer>
                    {authUser => 
                        needsEmailVerification(authUser) ? (
                            <div>
                                {this.state.isSent ? (
                                    <p>
                                        email confirmation sent: Check your email (spam folder included) for a confirmation email. refresh page if needed
                                    </p>
                                ) : (
                                    <p>
                                        Verify your E-mail: Check you E-Mails (Spam folder inlcuded) for a confirmation e-mail or send another confirmation email.
                                    </p>
                                )}
                                
                                <button
                                    type="button"
                                    onClick={this.onSendEmailVerification}
                                    disabled={this.state.isSent}
                                >
                                    Send Confirmation email
                                </button>
                            </div>
                        ) : (
                        <Component {...this.props}/>
                        )
                    }
                </AuthUserContext.Consumer>
            );
        }
    }

    return withFirebase(WithEmailVerification);
}

export default withEmailVerification;