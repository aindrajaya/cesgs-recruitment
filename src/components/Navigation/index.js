import React from 'react';
import {Link} from 'react-router-dom';

import SignOutButton from '../SignOut';
import * as ROUTES from '../../constants/routes';
import * as ROLES from '../../constants/roles';

import {AuthUserContext} from  '../Session';

import {Container, Menu} from 'semantic-ui-react';

const Navigation = ({authUser}) => (
    <div>    
        {/* with roles admin */}
        <AuthUserContext.Consumer>
            {authUser => 
                authUser ? (
                    <NavigationAuth authUser={authUser}/>
                ) : (
                    <NavigationNonAuth />
                )
            }
        </AuthUserContext.Consumer>
    </div>
);

//with admin roles
const NavigationAuth = ({authUser}) =>(
    <Menu pointing secondary>  
        <Container>
            <Menu.Item name="Landing" as={Link} to={ROUTES.LANDING}/>
            <Menu.Item name="home" as={Link} to={ROUTES.HOME}/>
            <Menu.Item name="Account" as={Link} to={ROUTES.ACCOUNT}/>
            {/* WITH ROLES ADMIN */}
            {!!authUser.roles[ROLES.ADMIN] && (
                <Menu.Item name="Admin" as={Link} to={ROUTES.ADMIN}/>
            )}
            <SignOutButton/>
        </Container> 
    </Menu>
);

const NavigationNonAuth = () => (
    <Menu pointing secondary>
        <Container>
            <Menu.Item name="home" as={Link} to={ROUTES.LANDING}/>
            <Menu.Menu>
                <Menu.Item name="signin" as={Link} to={ROUTES.SIGN_IN}/>
            </Menu.Menu>
        </Container>
    </Menu>
);

export default Navigation;