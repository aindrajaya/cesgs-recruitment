import React from 'react';
import { Segment, Dimmer, Loader } from 'semantic-ui-react';
const Loading = () => (
	<Segment className="fullscreen-segment">
		<Dimmer active inverted>
			<Loader size="large">Cargando...</Loader>
		</Dimmer>
	</Segment>
);

export default Loading;
