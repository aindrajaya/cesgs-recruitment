import React from 'react';
import Loading from './index';
import { shallow } from 'enzyme';

describe('<Loading />', () => {
	it ('should be mount correctly', () => {
		const wrapper = shallow(<Loading />);
		expect(wrapper.find('.fullscreen-segment').length).toEqual(1);
	});
});
