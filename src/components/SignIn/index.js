import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {compose} from 'recompose';

import {SignUpLink} from '../SignUp';
import {PasswordForgetLink} from '../PasswordForget';
import {withFirebase} from '../Firebase';
import * as ROUTES from '../../constants/routes';

import {
    Grid,
    Form,
    Button,
    Header,
    Icon,
    Message,
    Divider,
} from 'semantic-ui-react';

const SignInPage = () =>(
    <Grid centered columns={2}>
        <Grid.Column>
            <Header as="h2" textAlign="center">
                <h1>Sign In</h1>
            </Header>
            <SignInForm />
            <SignInGoogle/>
            <PasswordForgetLink />
            <SignUpLink />
        </Grid.Column>
    </Grid>
);

const INITIAL_STATE = {
    email: '',
    password: '',
    error: null,
};

class SignInFormBase extends Component{
    constructor(props){
        super(props);

        this.state= {...INITIAL_STATE};
    }

    onSubmit = event => {
        const {email, password} = this.state;

        this.props.firebase
            .doSignInWithEmailAndPassword(email, password)
            .then(() => {
                this.setState({...INITIAL_STATE});
                this.props.history.push(ROUTES.HOME);
            })
            .catch(error =>{
                this.setState({error});
            });

        event.preventDefault();
    };

    onChange = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    render(){
        const {email, password, error} = this.state;
        
        const isInvalid = password ==='' || email ==='';

        return(
            <div>
            {error && (
                <Message>
                    <p>{error.message}</p>
                </Message>
            )}
            <Form onSubmit={this.onSubmit}>
                <Form.Field>
                <label>Email</label>
                <input
                    name="email"
                    value={email}
                    onChange={this.onChange}
                    type="text"
                    placeholder="Email Address"
                />
                </Form.Field>
                <Form.Field>
                <label>Password</label>
                <input
                    name="password"
                    value={password}
                    onChange={this.onChange}
                    type="password"
                    placeholder="Password"
                />
                </Form.Field>      
                <Button primary disabled={isInvalid} type="submit">
                    Sign In
                </Button>
                <PasswordForgetLink />
                <Divider horizontal>Or sign in with</Divider>
            </Form>
            </div>
        );
    }
}

class SignInGoogleBase extends Component{
    constructor(props){
        super(props);

        this.state = {error: null};
    }

    onSubmit = event =>{
        this.props.firebase
            .doSignInWithGoogle()
            .then(socialAuthUser => {
                //user create
                return this.props.firebase
                .user(socialAuthUser.user.uid)
                .set({
                    username: socialAuthUser.user.displayName,
                    email: socialAuthUser.user.email,
                    // roles: {},
                });
            })
            .then(() => {
                this.setState({error: null});
                this.props.history.push(ROUTES.HOME);
            })
            .catch(error => {
                if(error.code === ERROR_CODE_ACCOUNT_EXISTS){
                    error.message = ERROR_MSG_ACCOUNT_EXISTS;
                }
                this.setState({error});
            });
        event.preventDefault();
    };

    render(){
        const {error} = this.state;
        return(
            <form onSubmit={this.onSubmit}>
                <Button color="google plus" type="submit">
                    <Icon name="google"/>Google
                </Button>

                {error && (
                <Message>
                    <p>{error.message}</p>
                </Message>)}
            </form>
        );
    }
}

const SignInForm = compose(
    withRouter,
    withFirebase,
)(SignInFormBase);

const SignInGoogle = compose(
    withRouter,
    withFirebase,
)(SignInGoogleBase);

const ERROR_CODE_ACCOUNT_EXISTS = 
    'auth/account-exists-with-different-credential';

const ERROR_MSG_ACCOUNT_EXISTS = 
    `And account with na E-Mail address to this social account already Exist. Please try another email acount`;


export default SignInPage;

export {SignInForm, SignInGoogle};