import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import {compose} from 'recompose';

import {withFirebase} from '../Firebase';
import * as ROUTES from '../../constants/routes';
import * as ROLES from '../../constants/roles';

import {
    Form,
    Button,
    Grid,
    Header,
    Message,
    Checkbox,
} from 'semantic-ui-react';

const SignUpPage = () =>(
    <Grid centered columns={2}>
        <Grid.Column>
            <Header as="h2" textAlign="center">Sign Up</Header>
            <SignUpForm />
        </Grid.Column>
    </Grid>
);


//with Admin role
const INITIAL_STATE = {
    username: '',
    email: '',
    passwordOne: '',
    passwordTwo: '',
    isAdmin: false,
    error: null,
};

class SignUpFormBase extends Component{
    constructor(props){
        super(props);

        this.state = {...INITIAL_STATE};
    }

    onSubmit = event => {        
        const {username, email, passwordOne, isAdmin} = this.state;
        const roles = {};

        if(isAdmin){
            roles[ROLES.ADMIN] = ROLES.ADMIN;
        }

        this.props.firebase
            .doCreateUserWithEmailAndPassword(email, passwordOne)
            .then(authUser => {
                //create user in firebase realtime db 
                return this.props.firebase
                .user(authUser.user.uid)
                .set({
                    username,
                    email,
                    roles,
                });
            })
            .then(() => {
                return this.props.firebase.doSendEmailVerification();
            })
            .then(() => {
                this.setState({...INITIAL_STATE});
                this.props.history.push(ROUTES.HOME);
            })
            .catch(error => {
                if(error.code === ERROR_CODE_ACCOUNT_EXISTS){
                    error.message = ERROR_MSG_ACCOUNT_EXISTS;
                }
                this.setState({error});
            });
        
        event.preventDefault();
    }

    onChange = event =>{
        this.setState({[event.target.name] : event.target.value});
    };

    //checkbok admin role
    onChangeCheckbox = event => {
        this.setState({[event.target.name]: event.target.checked});
    };

    render(){
        const{
            username,
            email,
            passwordOne,
            passwordTwo,
            isAdmin,
            error,
        } = this.state;

        const isInvalid = 
            passwordOne !== passwordTwo ||
            passwordTwo === '' ||
            email === '' ||
            username === '';

        return( 
            <div>
                <Form onSubmit={this.onSubmit}>
                    <Form.Field>
                    <label>Username</label>
                    <input 
                        name="username" 
                        value={username} 
                        onChange={this.onChange} 
                        type="text" 
                        placeholder="Full Name"
                    />
                    </Form.Field>
                   <Form.Field>
                    <label>Email</label>
                   <input 
                        name="email" 
                        value={email} 
                        onChange={this.onChange} 
                        type="text" 
                        placeholder="Email Address"
                    />
                   </Form.Field>
                    <Form.Group>
                        <Form.Field>
                        <label>Password</label>
                        <input 
                            name="passwordOne" 
                            value={passwordOne}
                            onChange={this.onChange} 
                            type="text" 
                            placeholder="Password"
                        />
                        </Form.Field>
                        <Form.Field>
                        <label>Confirm Password</label>
                        <input 
                            name="passwordTwo" 
                            value={passwordTwo} 
                            onChange={this.onChange} 
                            type="text" 
                            placeholder="Confirm Password"
                        />
                        </Form.Field>
                    </Form.Group>     
                    {/* <Form.Field> */}
                    {/* Admin form check */}
                    {/* <Checkbox
                        label="Admin"
                        name="isAdmin"
                        onChange={this.onChangeCheckbox}
                        checked={isAdmin}
                    />    */}
                    {/* </Form.Field> */}
                    <Button primary disabled={isInvalid} type="submit">
                        Sign Up
                    </Button> 
                </Form>
                {error && (
                <Message>
                    <p>{error.message}</p>
                </Message>
                )}
            </div>
        );
    }
}

const SignUpLink = () => (
    <p>
        Don't have an account? <Link to={ROUTES.SIGN_UP}>Sign Up</Link>
    </p>
);

const SignUpForm = compose(
    withRouter,
    withFirebase,
) (SignUpFormBase);

const ERROR_CODE_ACCOUNT_EXISTS = 
    'auth/account-exists-with-different-credential';

const ERROR_MSG_ACCOUNT_EXISTS = 
    `And account with na E-Mail address to this social account already Exist. Please try another email acount`;

export default SignUpPage;
export {SignUpForm, SignUpLink};