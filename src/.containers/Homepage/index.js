import React, { Component } from 'react';
import { Container, Header } from 'semantic-ui-react';
import Login from '../../components/Login';

class Homepage extends Component {
	createTreasure = () => {
		this.props.history.push('/treasure');
	}

	render() {
		return (
			<Container fluid className="container-homepage">
				<div className="homepage-intro">
					<Header as="h1">Crea tu búsqueda del tesoro</Header>
					<Header as="h4">Gratis y online</Header>
					<Login createTreasure={this.createTreasure}/>
				</div>
			</Container>
		);
	}
}

export default Homepage;
