import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Loading from '../../components/Loading';
import {
	Container,
	Header
} from 'semantic-ui-react';
import { treasuresRef } from '../../config/firebase';

class TreasureDetail extends Component {
	state = {
		treasure: null
	};

	componentDidMount() {
		this.getTreasure();
	}

	getTreasure = () => {
		const tid = this.props.match.params.id;
		treasuresRef.child(tid).once('value')
			.then((snap) => this.setState({ treasure: snap.val() }))
			.catch((err) => console.log('error to get a tresure: ', err));
	}

	render() {
		const { treasure } = this.state;
		if (!treasure) return <Loading />;
		return (
			<Container fluid className="container-treasure-detail">
				<Link to="/treasure">Volver a mis proyectos</Link>
				<Header as="h1">Treasure Detail Container</Header>
				<hr />
				<Header as="h3">{treasure.name}</Header>
				<p>{treasure.description}</p>
			</Container>
		)
	}
}

export default TreasureDetail;