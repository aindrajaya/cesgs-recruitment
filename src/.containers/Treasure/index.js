import React, { Component } from 'react';
import { Link } from "react-router-dom";
import {
	Container,
	Header,
	Button,
	Card,
	Modal,
	Input,
	TextArea,
	Form
} from 'semantic-ui-react';
import { Formik, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { treasuresRef, authRef } from "../../config/firebase";
import Loading from '../../components/Loading';

const newTreasureSchema = Yup.object().shape({
  name: Yup.string()
    .min(5, 'El nombre debe tener al menos 5 caracteres.')
    .max(35, 'El nombre excede el máximo de 35 caracteres.')
    .required('Este campo es requerido.')
});

class Treasure extends Component {
	state = {
		modalOpen: false,
		treasures: null
	}

	componentDidMount() {
		this.getTreasures();
	}

	getTreasures = () => {
		treasuresRef.once('value').then(snap => {
			const snapToArr = Object.values(snap.val());
			const filterSnap = snapToArr.filter(item => item.user === authRef.currentUser.email);
			this.setState({ treasures: filterSnap });
		});
	}

	openModal = () => this.setState({ modalOpen: true });
	closeModal = () => this.setState({ modalOpen: false });

	NewTreasureHTML = ({
		values,
		handleBlur,
		handleChange,
		handleSubmit,
	}) => (
		<Form onSubmit={handleSubmit}>
			<Form.Field>
				<label>Nombre:</label>
				<Input
					type="text"
					name="name"
					onChange={handleChange}
					onBlur={handleBlur}
					value={values.name}
					required
					placeholder='Cumpleaños de josé' />
				<ErrorMessage name="name" render={m => <div className="error">{m}</div>} />
			</Form.Field>
			<Form.Field>
				<label>Descripción:</label>
				<TextArea
					type="text"
					name="description"
					onChange={handleChange}
					onBlur={handleBlur}
					value={values.description}
					autoHeight
					placeholder='Esta es una dinámica para que josé encuentre su regalo de cumpleaños' />
				<ErrorMessage component="div" name="description" />
			</Form.Field>
			<Button type="submit" primary>Crear</Button>
		</Form>
	);
	
	ModalCreateTreasure = () => (
		<Modal
			onClose={this.closeModal}
			size="mini"
			closeIcon
			open={this.state.modalOpen}>
			<Modal.Header>Nueva búsqueda del tesoro</Modal.Header>
			<Modal.Content>
				<Modal.Description>
					<Header>Ingresa el título de tu proyecto</Header>
					<p>Al presionar <strong>crear</strong> podrás editar el proyecto</p>
					<Formik
						initialValues={{
							name: '',
							description: ''
						}}
						onSubmit={(values, {setSubmitting}) => {
							this.createNewTreasure(values);
							setSubmitting(false);
						}}
						validationSchema={newTreasureSchema}
						render={this.NewTreasureHTML}
						/>
				</Modal.Description>
			</Modal.Content>
		</Modal>
	);
	
	createNewTreasure = (values) => {
		const today = new Date().toISOString().slice(0, 10);
		const tkey = treasuresRef.push({ name: values.name }).key;
		const treasureObj = {
			id: tkey,
			name: values.name,
			description: values.description,
			created: today,
			user: authRef.currentUser.email
		};
		treasuresRef.child(tkey).set(treasureObj, (err) => {
			if (!err) {
				this.closeModal();
				this.getTreasures();
			}
		});
	}
	
	deleteTreasure = id => treasuresRef.child(id).remove((err) => {
		if (!err) this.getTreasures();
	});

  render(){
		if (!this.state.treasures) return <Loading />;
		return (
			<Container fluid className="container-treasure">
				<Header as="h1">Bienvenido al Home de Treasure</Header>
				<Link to="/">Volver a Inicio</Link>
				<br />
				<Button onClick={this.openModal}>Crear nuevo</Button>
				<this.ModalCreateTreasure />
				<hr />
				{this.state.treasures.length === 0 ?
					<Header as="h1">No hay proyectos creados</Header> : (
					<Card.Group>
						{this.state.treasures.map(treasure => (
							<Card key={treasure.name}>
								<Card.Content>
									<Card.Header>{treasure.name}</Card.Header>
									<Card.Meta>creado el {treasure.created}</Card.Meta>
									<Card.Description>
										{treasure.description}
									</Card.Description>
								</Card.Content>
								<Button.Group basic size='small' floated='right'>
									<Button
										icon='eye'
										onClick={() => this.props.history.push(`/treasure/${treasure.id}`)}/>
									<Button icon='share square' />
									<Button
										icon='trash alternate'
										onClick={() => this.deleteTreasure(treasure.id)} />
								</Button.Group>
							</Card>
						))}
					</Card.Group>)}
			</Container>
		)
	}
}

export default Treasure;